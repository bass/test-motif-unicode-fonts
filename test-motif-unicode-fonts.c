#include <stdlib.h>
#include <wchar.h>

#include <Xm/XmAll.h>

void addLine(Widget parent,
		char *keyWidgetName,
		char *keyLabel,
		char *valueWidgetName,
		const XmString valueLabel);

int main(int argc, char *argv[]) {
	XtAppContext appContext;

	XtSetLanguageProc(NULL, (XtLanguageProc) NULL, (XtPointer) NULL);

	const char *applicationClass = "XmUnicodeTest";
	String fallbackResources[] = {
			"*.labelSingleByte.fontList:	-monotype-arial-medium-r-normal--*-90-*-*-p-*-microsoft-cp1251",
			"*.labelSingleByte.foreground:	red",
			"*.labelUnicode.fontList:	-monotype-arial-medium-r-normal--*-90-*-*-p-*-iso10646-1",
			"*.labelUnicode.foreground:	green",
			"*.labelLocale.fontList:	-monotype-arial-medium-r-normal--*-90-*-*-p-*-*-*",
			"*.labelLocale.foreground:	blue",
			NULL,
	};
	const Widget topLevel = XtVaAppInitialize(&appContext, applicationClass, NULL, 0, &argc, argv, fallbackResources, NULL);

	XtSetLanguageProc(appContext, (XtLanguageProc) NULL, (XtPointer) NULL);

	const Widget mainWindow = XmCreateMainWindow(topLevel, "mainWindow", NULL, 0);
	XtManageChild(mainWindow);

	const Widget rowColumn = XmCreateRowColumn(mainWindow, "rowColumn", NULL, 0);
	XtManageChild(rowColumn);

	XtVaSetValues(rowColumn, XmNpacking, XmPACK_COLUMN, NULL);
	XtVaSetValues(rowColumn, XmNorientation, XmHORIZONTAL, NULL);

	const XmStringCharSet unicodeCharset = "UTF-8";

	char *mbs = "Latin-1: \u00c0\u00c1\u00c2\u00c3\u00c4\u00c5\u00e0\u00e1\u00e2\u00e3\u00e4\u00e5; Cyrillic: \u0410\u0411\u0412\u0413\u0430\u0431\u0432\u0433";
	wchar_t *wcs = L"Latin-1: \u00c0\u00c1\u00c2\u00c3\u00c4\u00c5\u00e0\u00e1\u00e2\u00e3\u00e4\u00e5; Cyrillic: \u0410\u0411\u0412\u0413\u0430\u0431\u0432\u0433";
	wchar_t *wcs2 = L"Cyrillic: ��������";

	char *singleByteLabelClass = "labelSingleByte";
	char *unicodeLabelClass = "labelUnicode";
	char *localeLabelClass = "labelLocale";

	int columnCount = 0;

	{
		const XmString labelString = XmStringCreate(mbs, unicodeCharset);
		addLine(rowColumn, "key", "XmStringCreate(multi-byte string, \"UTF-8\"), UTF-8 font:", unicodeLabelClass, labelString);
		XmStringFree(labelString);
		columnCount++;
	}

#if 0 // UTF-8 locale only
	{
		const XmString labelString = XmStringCreate(mbs, XmSTRING_DEFAULT_CHARSET);
		addLine(rowColumn, "key", "XmStringCreate(multi-byte string, \"UTF-8\"), UTF-8 font:", unicodeLabelClass, labelString);
		XmStringFree(labelString);
		columnCount++;
	}
#endif

	{
		const XmString labelString = XmStringCreate(mbs, unicodeCharset);
		addLine(rowColumn, "key", "XmStringCreate(multi-byte string, \"UTF-8\"), UTF-8 font:", localeLabelClass, labelString);
		XmStringFree(labelString);
		columnCount++;
	}

	{
		const XmString labelString = XmStringCreate(mbs, XmSTRING_DEFAULT_CHARSET);
		addLine(rowColumn, "key", "XmStringCreate(multi-byte string, \"UTF-8\"), UTF-8 font:", localeLabelClass, labelString);
		XmStringFree(labelString);
		columnCount++;
	}

	{
		const XmString labelString = XmStringCreate(mbs, _MOTIF_DEFAULT_LOCALE);
		addLine(rowColumn, "key", "XmStringCreate(multi-byte string, \"UTF-8\"), UTF-8 font:", localeLabelClass, labelString);
		XmStringFree(labelString);
		columnCount++;
	}


	{
		const XmString labelString = XmStringGenerate(mbs, NULL, XmMULTIBYTE_TEXT, NULL);
		addLine(rowColumn, "key", "XmStringGenerate(XmMULTIBYTE_TEXT):", localeLabelClass, labelString);
		XmStringFree(labelString);
		columnCount++;
	}

#if 0 // UTF-8 locale only
	{
		const XmString labelString = XmStringGenerate(mbs, NULL, XmMULTIBYTE_TEXT, NULL);
//		const XmString labelString = XmStringGenerate(mbs, _MOTIF_DEFAULT_LOCALE, XmMULTIBYTE_TEXT, NULL);
		addLine(rowColumn, "key", "XmStringGenerate(XmMULTIBYTE_TEXT):", unicodeLabelClass, labelString);
		XmStringFree(labelString);
		columnCount++;
	}
#endif

	{
		const XmString labelString = XmStringGenerate(mbs, _MOTIF_DEFAULT_LOCALE, XmMULTIBYTE_TEXT, NULL);
		addLine(rowColumn, "key", "XmStringGenerate(XmMULTIBYTE_TEXT):", localeLabelClass, labelString);
		XmStringFree(labelString);
		columnCount++;
	}


	{
		const XmString labelString = XmStringGenerate(wcs, NULL, XmWIDECHAR_TEXT, NULL);
//		const XmString labelString = XmStringGenerate(wcs, _MOTIF_DEFAULT_LOCALE, XmWIDECHAR_TEXT, NULL);
		addLine(rowColumn, "key", "XmStringGenerate(XmWIDECHAR_TEXT):", localeLabelClass, labelString);
		XmStringFree(labelString);
		columnCount++;
	}
	{
		const XmString labelString = XmStringGenerate(wcs, NULL, XmWIDECHAR_TEXT, NULL);
//		const XmString labelString = XmStringGenerate(wcs, _MOTIF_DEFAULT_LOCALE, XmWIDECHAR_TEXT, NULL);
		addLine(rowColumn, "key", "XmStringGenerate(XmWIDECHAR_TEXT):", singleByteLabelClass, labelString);
		XmStringFree(labelString);
		columnCount++;
	}
	{
		const XmString labelString = XmStringGenerate(wcs, NULL, XmWIDECHAR_TEXT, NULL);
//		const XmString labelString = XmStringGenerate(wcs, _MOTIF_DEFAULT_LOCALE, XmWIDECHAR_TEXT, NULL);
		addLine(rowColumn, "key", "XmStringGenerate(XmWIDECHAR_TEXT):", unicodeLabelClass, labelString);
		XmStringFree(labelString);
		columnCount++;
	}


	XtVaSetValues(rowColumn, XmNnumColumns, columnCount, NULL); // For XmHORIZONTAL orientation, this is actually a number of rows

	XtRealizeWidget(topLevel);
	XtAppMainLoop(appContext);

	return 0;
}

void addLine(Widget parent,
		char *keyWidgetName,
		char *keyLabel,
		char *valueWidgetName,
		const XmString valueLabel) {
	const Widget label0 = XmCreateLabel(parent, keyWidgetName, NULL, 0);
	XtManageChild(label0);
	const XmString labelString0 = XmStringCreate(keyLabel, XmSTRING_DEFAULT_CHARSET);
	XtVaSetValues(label0, XmNlabelString, labelString0, NULL);
	XmStringFree(labelString0);

	const Widget label1 = XmCreateLabel(parent, valueWidgetName, NULL, 0);
	XtManageChild(label1);
	XtVaSetValues(label1, XmNlabelString, valueLabel, NULL);
}
